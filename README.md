**Lazymikrotik** merupakan aplikasi sederhana untuk setting routerboard mikrotik berhubung saya malas untuk setting mikrotik berulang-ulang, walaupun ada fitur restore tapi tetep ada aja yang harus dirubah jadi akhirnya memutuskan buat aplikasi ini.

**Lazymikrotik** akan menghasilkan output berupa file **"backup.rsc"****. Format .rsc sama dengan format .backup bedanya cuma di .backup isi sudah di compile oleh mikrotik sedangkan .rsc masih versi rawnya.


**Cara pakai :**

1. Seret file backup.rsc ke files di winbox
2. Buka terminal lalu ketikkan **import file-name="backup.rsc"

**Fitur yang ada saat ini:**

- Add ip address
- Add ip route
- Add simple queue
- Add dns
- Set identity
- Add Address List Firewall

Untuk settingan sisanya masih pakai settingan default kantor saya bekerja. Mungkin entar dibuat aplikasi sett manual tiap fungsi dari mikrotik