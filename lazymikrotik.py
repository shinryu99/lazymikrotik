import os

exit = 0
x = 0

while(exit==0):

    os.system('cls')

    if x == 0:
        f = open('backup.rsc', 'w')
        f.write("\n/ip address remove number=0\n")
        x+= 1

    print("1. Add IP Address")
    print("2. Set Identity")
    print("3. Add Simple Queue")
    print("4. Add IP Route")
    print("5. Add DNS")
    print("0. Exit")

    pil = input("Masukkan Pilihan = ")

    if pil == 0:

        f.write("\n/ip service\nset www port=88\nset api disabled=yes")
        f.write("\n/system watchdog\nset watchdog-timer=no")
        f.write("\n/tool bandwidth-server\nset max-sessions=10")
        f.write("\n/system logging\nset 1 action=disk\nadd action=disk topics=critical")
        f.write("\n/system clock\nset time-zone-autodetect=no time-zone-name=Asia/jakarta")
        f.write("\n/system clock manual\nset dst-delta=+01:00 time-zone=+07:00")
        f.write("\n/ip firewall nat\nadd action=masquerade chain=srcnat src-address-list=src_address to-addresses=\ \n 0.0.0.0")
        f.write("\n/user\nremove numbers=0")
        f.write("\n/user\nadd name=delta password=d3lt@X99 group=full")

        exit = 1

    elif pil == 1:

        ip = raw_input("Masukkan Ip Address = ")
        network = raw_input("Masukkan Network Address = ")

        slash = input("Masukkan Slash = ")
        comment = raw_input("Masukkan Comment = ")

        print("\n1. Ether 1")
        print("2. Ether 2")
        print("3. Ether 3")
        print("4. Ether 4")
        print("5. Ether 5")

        interface = raw_input("Silahkan Pilih Interface = ")

        f.write("\n/ip address\n add address %s/%d" % (ip, slash))

        if interface == 1:
            f.write(" interface=ether1")
        elif interface == 2:
            f.write(" interface=ether2")
        elif interface == 3:
            f.write(" interface=ether3")
        elif interface == 4:
            f.write(" interface=ether4")
        elif interface == 5:
            f.write(" interface=ether5")

        f.write(" network=%s comment=%s" %(network,comment))

        exit = 0

    elif pil == 2:

        identity = raw_input("Masukkan Identity Name = ")

        f.write("\n/system identity\n set name=%s" % (identity))

        exit = 0

    elif pil == 3:

        ip_simple_queue_arr = []
        ip_simple_queue = raw_input("Masukkan IP Address PC Warnet = ")
        jumlah_queue = input("Masukkan Jumlah PC = ")
        max_limit = input("Masukkan Max Limit Upload / Download = ")

        ip_simple_queue_arr.extend(ip_simple_queue.split('.'))
        ip_simple_queue_end = int(ip_simple_queue_arr[3])

        f.write("\n/queue simple\n")

        for i in range(ip_simple_queue_end, jumlah_queue+1):
            f.write("\nadd max-limit=%dk/%dk name=PC%d target=%s.%s.%s.%d" % (max_limit,max_limit,i,ip_simple_queue_arr[0],ip_simple_queue_arr[1],ip_simple_queue_arr[2],i))

        f.write("\nadd target=%s.%s.%s.0/24 name=block max-limit=%dk/%dk disabled=yes" % (ip_simple_queue_arr[0],ip_simple_queue_arr[1],ip_simple_queue_arr[2],max_limit,max_limit))
        f.write("\nadd name=Billing parent=\"Browsing Packet\" queue=default/default target=%s.%s.%s.254" % (ip_simple_queue_arr[0],ip_simple_queue_arr[1],ip_simple_queue_arr[2]))
        f.write("\n/ip firewall address-list")

        for i in range(ip_simple_queue_end, jumlah_queue+1):
            f.write("\nadd address=%s.%s.%s.%d list=src_address" % (ip_simple_queue_arr[0],ip_simple_queue_arr[1],ip_simple_queue_arr[2],i))

        f.write("\nadd address=%s.%s.%s.0/24 list=block_ip " % (ip_simple_queue_arr[0],ip_simple_queue_arr[1],ip_simple_queue_arr[2]))

        exit = 0

    elif pil == 4:

        ip_route_gateway = raw_input("Masukkan IP Radio = ")

        f.write("\n/ip route\nadd distance=1 gateway=%s" % ip_route_gateway)

        exit = 0

    elif pil == 5:

        ip_dns1 = raw_input("Masukkan IP DNS1 = ")
        ip_dns2 = raw_input("Masukkan IP DNS2 = ")

        f.write("\n/ip dns\nset allow-remote-request=yes max-udp-packet-size=512 servers=%s,%s/" %(ip_dns1,ip_dns2))
        f.write("\n/ip firewall address-list")
        f.write("\nadd address=%s list=dns" % (ip_dns1))
        f.write("\nadd address=%s list=dns" % (ip_dns2))

        exit = 0
